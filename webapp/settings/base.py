import os

from pathlib import Path

from dotenv import load_dotenv, find_dotenv
from envparse import env

try:
    load_dotenv(find_dotenv(raise_error_if_not_found=True))
except IOError as e:
    print('No .env file found, using defaults.')

ROOT_DIR = Path().resolve()
FIXTURES_DIR = ROOT_DIR / 'fixtures'

# Where the model server will be listening
TRANSLATE_HOST = os.getenv('TRANSLATE_HOST', '0.0.0.0')
TRANSLATE_PORT = env.int('TRANSLATE_PORT', default=5005)

# Database URI
# Database URI
DB_PASSWORD = os.getenv('DATABASE_PASSWORD', 'secret')
SQLALCHEMY_DATABASE_URI = os.getenv('DATABASE_URI',
                                    'postgresql://postgres:%s@postgres/postgres' %
                                    DB_PASSWORD)
SQLALCHEMY_TRACK_MODIFICATIONS = False


# Third party services
USER_ENABLE_EMAIL = False  # Disable email authentication
USER_ENABLE_USERNAME = True  # Enable username authentication
USER_REQUIRE_RETYPE_PASSWORD = True  # Simplify register form

ADMIN_USER = 'admin'
ADMIN_PASSWORD = 'Qwerty123'

CREATE_ADMIN = True
TESTING = False

SECRET_KEY = "acnkla9999qeacno5kcbasfrtgtrgtrgdvsdvblrsivbsevisekqwl"
SESSION_TYPE = 'memcached'
