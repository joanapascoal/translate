from webapp.settings.base import *  # noqa

SQLALCHEMY_DATABASE_URI = 'postgresql://postgres:postgres@postgres/postgres'

REDIS_HOST = 'localhost'

SECRET_KEY = "acnkla9999qeacno5kcbasdvsdvblrsivbsevisekqwl"

ADMIN_USER = 'admin'
ADMIN_PASSWORD = 'Querty123'

CREATE_ADMIN = True

# Bcrypt algorithm hashing rounds (reduced for testing purposes only!)
BCRYPT_LOG_ROUNDS = 4

# Enable the TESTING flag to disable the error catching during request handling
# so that you get better error reports when performing test requests against the application.
TESTING = True

# Disable CSRF tokens in the Forms (only valid for testing purposes!)
WTF_CSRF_ENABLED = False
