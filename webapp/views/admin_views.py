from flask_admin import AdminIndexView
from flask_admin.contrib.sqla import ModelView
from flask_user import current_user


class DashboardModelView(ModelView):
    def is_accessible(self):
        return current_user.is_authenticated and current_user.has_roles('Admin')

    can_delete = False  # disable model deletion
    can_create = False
    can_edit = False
    can_export = True
    page_size = 50  # the number of entries to display on the list view


class HomeView(AdminIndexView):
    def is_accessible(self):
        return current_user.is_authenticated and current_user.has_roles('Admin')
