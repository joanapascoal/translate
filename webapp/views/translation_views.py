from flask import Blueprint, render_template, request, redirect
from flask_user import login_required, current_user

from webapp.external_services.unbabel_api import request_babel_translation
from webapp.models import Translation, update_translation_model, save_translation

translation_bp = Blueprint('translation_bp', __name__, template_folder='templates')
translation_bp.config = {}


@translation_bp.record_once
def record_params(state):
    """Store app.config in blueprint when blueprint is registered on app."""
    app = state.app
    translation_bp.config = app.config


@translation_bp.route('/translation', methods=['GET', 'POST'])
@login_required
def translate():
    if request.method == 'POST':
        new_translation = request_babel_translation(request.form.get('text'), 'en', 'es',
                                                    translation_bp.config['CALLBACK_URL'])
        save_translation(new_translation)
        return redirect('/translations_list')
    else:
        return render_template('translation/translation.html')


@translation_bp.route('/translations_list', methods=['GET'])
@login_required
def translation_list():
    transl_list = Translation.query.filter_by(owner_id=current_user.id).order_by(
        Translation.size.desc()).all()
    return render_template('translation/translations_list.html', translation_list=transl_list)


@translation_bp.route('/update_translation', methods=['POST'])
def update_translation():
    update_translation_model(request.form)
    return request.form.get('status')


@translation_bp.route('/get_translation/<string:translation_uid>', methods=['GET'])
def get_translation(translation_uid):
    detail = Translation.query.filter_by(uid=translation_uid).first()
    return render_template('translation/translation_detail.html', translation=detail)
