from flask import Blueprint, render_template


users_bp = Blueprint('users_bp', __name__, template_folder='templates')


@users_bp.route('/')
def home():
    return render_template('home.html')
