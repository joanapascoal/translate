from flask_sqlalchemy import SQLAlchemy
from flask_user import UserMixin, current_user
from sqlalchemy_utils.types.choice import ChoiceType

from webapp.external_services.unbabel_api import request_words

db = SQLAlchemy()


class User(db.Model, UserMixin):
    __tablename__ = 'users'
    id = db.Column(db.Integer, primary_key=True)
    active = db.Column('is_active', db.Boolean(), nullable=False, server_default='1')

    username = db.Column(db.String(255), nullable=False, unique=True)
    password = db.Column(db.String(255), nullable=False, server_default='')

    # User information
    first_name = db.Column(db.String(100), nullable=False, server_default='')
    last_name = db.Column(db.String(100), nullable=False, server_default='')
    translations = db.relationship('Translation', backref='user', lazy='dynamic')

    # Define the relationship to Role via UserRoles
    roles = db.relationship('Role', secondary='user_roles')


# Define the Role data-model
class Role(db.Model):
    __tablename__ = 'roles'
    id = db.Column(db.Integer(), primary_key=True)
    name = db.Column(db.String(50), unique=True)


# Define the UserRoles association table
class UserRoles(db.Model):
    __tablename__ = 'user_roles'
    id = db.Column(db.Integer(), primary_key=True)
    user_id = db.Column(db.Integer(), db.ForeignKey('users.id', ondelete='CASCADE'))
    role_id = db.Column(db.Integer(), db.ForeignKey('roles.id', ondelete='CASCADE'))


class Translation(db.Model):
    STATUS = [
        ('new', 'new'),
        ('translating', 'translating'),
        ('completed', 'completed'),
        ('failed', 'failed'),
        ('canceled', 'canceled'),
        ('accepted', 'accepted'),
        ('rejected', 'rejected')
    ]
    __tablename__ = 'translations'
    uid = db.Column(db.String, primary_key=True, nullable=False)
    original_text = db.Column(db.Text, nullable=False)
    translated_text = db.Column(db.Text, nullable=True)
    original_language = db.Column(db.String, nullable=False)
    translated_language = db.Column(db.String, nullable=False)
    size = db.Column(db.Integer, nullable=False)
    status = db.Column(ChoiceType(STATUS))
    owner_id = db.Column(db.Integer, db.ForeignKey('users.id', ondelete='CASCADE'))
    created_on = db.Column(db.DateTime, server_default=db.func.now())
    updated_on = db.Column(db.DateTime, server_default=db.func.now(),
                           server_onupdate=db.func.now())


def save_translation(new_translation):
    translation = Translation(uid=new_translation.uid, original_text=new_translation.text,
                              original_language=new_translation.source_language,
                              translated_language=new_translation.target_language,
                              status=new_translation.status,
                              size=0,
                              owner_id=current_user.id
                              )

    db.session.add(translation)
    db.session.commit()


def update_translation_model(updated_translation):
    translation = Translation.query.filter_by(uid=updated_translation.get('uid')).first()
    translation.status = updated_translation.get('status')
    translation.translated_text = updated_translation.get('translated_text')
    translation.size = request_words(updated_translation.get('translated_text'))

    db.session.commit()


def patch_status_translations_model(list_translations):
    uid_list = [t.uid for t in list_translations]
    transl_to_update = Translation.query.filter(Translation.uid.in_(uid_list))
    for transl in transl_to_update:
        transl.status = [x.status for x in list_translations if x.uid == transl.uid]
        db.session.commit()
