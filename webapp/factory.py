import os

from flask import Flask, render_template
from flask_admin import Admin
from flask_bootstrap import Bootstrap
from flask_user import UserManager

from webapp.models import db, User, Translation, Role
from webapp.views.admin_views import DashboardModelView, HomeView
from webapp.views.translation_views import translation_bp
from webapp.views.user_views import users_bp


def create_app():
    print('Running with settings from: {}'
          .format(os.environ['TRANSLATE_APP_SETTINGS_FILE']))

    flask_app = Flask(__name__)
    flask_app.config.from_envvar('TRANSLATE_APP_SETTINGS_FILE')
    flask_app.secret_key = flask_app.config['SECRET_KEY']

    create_admin_dashboard(flask_app)

    Bootstrap(flask_app)

    flask_app.register_blueprint(translation_bp)
    flask_app.register_blueprint(users_bp)

    flask_app.register_error_handler(404, page_not_found)
    flask_app.register_error_handler(403, page_forbidden)

    with flask_app.app_context():
        user_manager = UserManager(flask_app, db, User)
        db.init_app(flask_app)  # Required by Flask-SQLAlchemy
        if not flask_app.config['TESTING']:
            db.create_all()
        if flask_app.config['CREATE_ADMIN']:
            create_admin_user(flask_app, user_manager)

    return flask_app


def create_admin_dashboard(flask_app):
    admin = Admin(flask_app, name='dashboard', template_mode='bootstrap3', index_view=HomeView())
    admin.add_view(DashboardModelView(User, db.session))
    admin.add_view(DashboardModelView(Translation, db.session))


def create_admin_user(flask_app, user_manager):
    admin_role = Role.query.filter_by(name='Admin').first()
    if not admin_role:
        admin_role = Role(name='Admin')
        db.session.commit()

    user = User.query.filter_by(username=flask_app.config['ADMIN_USER']).first()
    if not user:
        user1 = User(username=flask_app.config['ADMIN_USER'], active=True,
                     password=user_manager.hash_password(flask_app.config['ADMIN_PASSWORD']))
        user1.roles = [admin_role, ]
        db.session.add(user1)
        db.session.commit()


def page_not_found(e):
    return render_template('errors/404.html'), 404


def page_forbidden(e):
    return render_template('errors/403.html'), 403
