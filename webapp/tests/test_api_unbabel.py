from webapp.external_services.unbabel_api import request_babel_translation, request_words


def test_save_new_translation():
    text = "something"
    translation = request_babel_translation(text, 'en', 'es', 'https://fd0d11b5.ngrok.io')
    assert translation.text == text
    assert translation.source_language == 'en'
    assert translation.target_language == 'es'
    assert translation.status == 'new'


def test_request_words():
    text = "Some Ideas"
    resp = request_words(text)
    assert resp is 2
