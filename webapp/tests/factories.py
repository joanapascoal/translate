from factory import SubFactory
from factory.alchemy import SQLAlchemyModelFactory

from webapp.models import Translation, db, UserRoles, Role, User


class UserFactory(SQLAlchemyModelFactory):
    class Meta:
        model = User
        sqlalchemy_session = db.session
        sqlalchemy_session_persistence = 'commit'

    active = True
    username = 'test'
    password = 'test'


class RoleFactory(SQLAlchemyModelFactory):
    class Meta:
        model = Role
        sqlalchemy_session = db.session
        sqlalchemy_session_persistence = 'commit'

    name = 'Admin'


class UserRoleFactory(SQLAlchemyModelFactory):
    class Meta:
        model = UserRoles
        sqlalchemy_session = db.session
        sqlalchemy_session_persistence = 'commit'

    user_id = SubFactory(UserFactory)
    role_id = SubFactory(RoleFactory)


class TranslationFactory(SQLAlchemyModelFactory):
    class Meta:
        model = Translation
        sqlalchemy_session = db.session
        sqlalchemy_session_persistence = 'commit'

    uid = '08ada102b1'
    original_text = 'test'
    translated_text = db.Column(db.Text, nullable=True)
    original_language = 'en'
    translated_language = 'es'
    size = 4
    status = 'completed'
    owner_id = SubFactory(UserFactory)
