import os

from flask import Flask
from flask_sqlalchemy import SQLAlchemy

from webapp.factory import create_app
from webapp.models import db, User, Role

from flask_testing import TestCase

from webapp.tests.utils import FlaskClient
from webapp.views.translation_views import translation_bp
from webapp.views.user_views import users_bp


class TestAPI(TestCase):

    def create_app(self):
        os.environ['TRANSLATE_APP_SETTINGS_FILE'] = 'settings/test.py'
        app = Flask(__name__)
        app.test_client_class = FlaskClient

        app.register_blueprint(translation_bp)
        app.register_blueprint(users_bp)

        with app.app_context():
            db = SQLAlchemy(app)
            db.init_app(app)

        return app

    def setUp(self):
        db.session.close()
        db.drop_all()
        db.create_all()

        self.client = self.create_app().test_client()

        self.default_role = Role(name=('normal'))
        db.session.add(self.default_role)
        db.session.commit()

        user = User(username='test', password='test')
        user.roles = [self.default_role, ]
        db.session.add(user)

        db.session.commit()

        self.user = user

        assert user in db.session

    def test_user_exists(self):
        self.assertTrue(
            db.session.query(User).filter(User.username == 'test').first()
            is not None)

    def test_get_translation_not_loggedin(self):
        with create_app().test_client() as c:
            resp = c.get('/translation')
            assert resp.status_code == 302
            assert resp.location == 'http://localhost/user/sign-in?next=/translation'

    def test_get_translations_list_not_loggedin(self):
        with create_app().test_client() as c:
            resp = c.get('/translations_list')
            assert resp.status_code == 302
            assert resp.location == 'http://localhost/user/sign-in?next=/translations_list'

    def test_404(self):
        resp = self.client.get('/other')
        self.assertEqual(resp.status, '404 NOT FOUND')

    def tearDown(self):
        db.session.remove()
        db.drop_all()
