import flask
from flask.testing import FlaskClient as BaseFlaskClient

# Based on https://gist.github.com/singingwolfboy/2fca1de64950d5dfed72
from flask_wtf.csrf import generate_csrf


class RequestShim(object):
    """
    A fake request that proxies cookie-related methods to a Flask test client.
    """

    def __init__(self, client):
        self.client = client
        self.vary = set({})

    def set_cookie(self, key, value='', *args, **kwargs):
        "Set the cookie on the Flask test client."
        server_name = "localhost"
        return self.client.set_cookie(
            server_name, key=key, value=value
        )

    def delete_cookie(self, key, *args, **kwargs):
        "Delete the cookie on the Flask test client."
        server_name = flask.current_app.config["SERVER_NAME"] or "localhost"
        return self.client.delete_cookie(
            server_name, key=key
        )


class FlaskClient(BaseFlaskClient):
    @property
    def csrf_token(self):
        request = RequestShim(self)
        environ_overrides = {}
        self.cookie_jar.inject_wsgi(environ_overrides)
        with flask.current_app.test_request_context(
                "/login", environ_overrides=environ_overrides,
        ):
            csrf_token = generate_csrf()
            flask.current_app.save_session(flask.session, request)
            return csrf_token

    def login(self, username, password):
        return self.post("/user/sign-in", data={
            "username": username,
            "password": password,
            "csrf_token": self.csrf_token,
        }, follow_redirects=True)

    def logout(self):
        return self.get("/user/sign-out", follow_redirects=True)
