from unbabel.api import UnbabelApi

uapi = UnbabelApi('fullstack-challenge', '9db71b322d43a6ac0f681784ebdcc6409bb83359',
                  sandbox=True)


def request_babel_translation(to_translate, source_language, target_language, callback_url):
    endpoint = '/update_translation'

    response = uapi.post_translations(text=to_translate, source_language=source_language,
                                      target_language=target_language,
                                      callback_url=callback_url + endpoint)
    return response


def request_words(text) -> int:
    response = uapi.get_word_count(text=text)

    return response


def request_status(status):
    response = uapi.get_translations(status=status)
    return response
