FROM python:3.6-slim as dependencies

# Set environment varibles
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

# Install prerequisites
RUN apt-get update && apt-get install -y \
    software-properties-common \
    curl
CMD /bin/bash

# We need new PostgreSQL Client versions and the one in Debian 9 is old
RUN mkdir /usr/share/man/man1 && mkdir /usr/share/man/man7 && \
    apt-get update && apt-get install -y wget gnupg && \
    touch /etc/apt/sources.list.d/pgdg.list && \
    echo "deb http://apt.postgresql.org/pub/repos/apt/ stretch-pgdg main" >> /etc/apt/sources.list.d/pgdg.list && \
    wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | apt-key add - && \
    apt-get update && apt-get install -y \
    build-essential \
    git-core \
    postgresql-client-10 \
    ipython \
    locales -qq && \
    dpkg-reconfigure locales && locale-gen C.UTF-8 && locale


# Set work directory
WORKDIR /code

# Install dependencies
RUN pip install --upgrade pip
RUN pip install pipenv
COPY ./Pipfile /code/Pipfile
RUN pipenv install --deploy --system --skip-lock --dev

# Copy project
COPY . /code/
