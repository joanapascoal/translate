# coding=utf-8
# Set the settings file to the local version and run the app.
import os
from webapp import factory

os.environ['TRANSLATE_APP_SETTINGS_FILE'] = 'settings/local.py'

app = factory.create_app()

if __name__ == '__main__':
    app.run(host=app.config['TRANSLATE_HOST'], port=int(app.config['TRANSLATE_PORT']))
