## Table of Contents

1. [Implementation](#implementation)
1. [Installation](#installation)
1. [Usage](#usage)
1. [Features](#features)
1. [Demo](#demo)
1. [License](#license)

- This application get information from an external API: https://developers.unbabel.com

## Implementation

### Installation

1 - Clone de project
```
git clone git@gitlab.com:joanapascoal/translate.git
```

2 - Please make sure you have a .env. 

3 - Install dependencies
```
docker-compose -f docker-compose.yml build --force-rm
```

4 - Run
```
docker-compose -f docker-compose.yml up --force-recreate
```

5 - At this point, website should be running on http://localhost:5005

You should add a valid callback_url on .env file, otherwise, the translations don't will be updated (e.g. ngrok).

### Usage

1 - Open a new terminal to install dependencies on virtualenv:
```
pipenv install
```

2 - Run tests with ``` pytest ``` command

3 - To Run Coverage
```
coverage run -m pytest
coverage report
```

3 - Go to http://localhost:5005/admin after authenticate yourself with credentials in .env to access Admin dashboard 

## Demo

**[Back to top](#table-of-contents)**

* Home



## License

#### (The MIT License)

Copyright (c) 2014 Bill Gooch

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
'Software'), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
        distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

        The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

        THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND,
        EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
        IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
        TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

**[Back to top](#table-of-contents)**




